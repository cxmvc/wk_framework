/*
  +----------------------------------------------------------------------+
  | PHP Version 5                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2013 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
*/

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "main/SAPI.h"  
#include "Zend/zend_interfaces.h"  
#include "ext/standard/php_var.h"  
#include "ext/standard/php_string.h"  
#include "ext/standard/php_smart_str.h"  
#include "ext/standard/url.h"
#include "ext/standard/php_string.h"
#include "php_wk_framework.h"
#include "wk_model.h"
#include "wk_controller.h"
#include "wk_input.h"

zend_class_entry *wk_input_ce;
static int le_wk_input;

ZEND_BEGIN_ARG_INFO_EX(void_arginfo, 0, 0, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(get_arginfo, 0, 0, 1)  
ZEND_ARG_INFO(0, name)
ZEND_ARG_INFO(0, default)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(post_arginfo, 0, 0, 1)  
ZEND_ARG_INFO(0, name)
ZEND_ARG_INFO(0, default)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(get_post_arginfo, 0, 0, 1)  
ZEND_ARG_INFO(0, name)
ZEND_ARG_INFO(0, default)
ZEND_END_ARG_INFO()

PHP_METHOD(wk_input, __construct){
}

/*wk_input::get(name, default="")*/
PHP_METHOD(wk_input, get){
    zval *name, *default_val, *ret;

    if( zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &name, &default_val) == FAILURE ){
        RETURN_NULL();
    }
    
    if(Z_TYPE_P(name) != IS_STRING){
        convert_to_string(name);
    }

    ret = request_var(GET, Z_STRVAL_P(name), Z_STRLEN_P(name));

    RETURN_ZVAL(ret, 1, 0);
}

/*wk_input::post(name)*/
PHP_METHOD(wk_input, post){
    zval *name, *ret;

    if( zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &name) == FAILURE ){
        RETURN_NULL();
    }

    if(Z_TYPE_P(name) != IS_STRING){
        convert_to_string(name);
    }

    ret = request_var(POST, Z_STRVAL_P(name), Z_STRLEN_P(name));

    RETURN_ZVAL(ret, 1, 0);
}

/*wk_input::get_post(name)*/
PHP_METHOD(wk_input, get_post){
    zval *name, *default_val, *ret;

    if( zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "z", &name) == FAILURE ){
        RETURN_NULL();
    }

    ret = request_var(REQUEST, Z_STRVAL_P(name), Z_STRLEN_P(name));

    RETURN_ZVAL(ret, 1, 0);
}

static zend_function_entry wk_input_method[] = {
    ZEND_ME(wk_input, __construct, void_arginfo, ZEND_ACC_CTOR|ZEND_ACC_PUBLIC)
    ZEND_ME(wk_input, get, get_arginfo, ZEND_ACC_PUBLIC)
    ZEND_ME(wk_input, post, post_arginfo, ZEND_ACC_PUBLIC)
    ZEND_ME(wk_input, get_post, get_post_arginfo, ZEND_ACC_PUBLIC)
    {NULL, NULL, NULL}
};
 
EXT_STARTUP_FUNCTION(wk_input){
    zend_class_entry wk_input;
    INIT_CLASS_ENTRY(wk_input, "wk_input", wk_input_method);
    wk_input_ce = zend_register_internal_class(&wk_input TSRMLS_CC);
    return SUCCESS;
}