<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 数据库模型 first
 *
 * @version Id:  base_model.php 下午2:02:42 2013-1-16   $
 */

class db_model extends WK_Model{
	public $db;
	
	public function __construct(){
		
	}
	
	/**
	 * 获取表记录第一条
	 * @param string $tablename
	 * @param string $order
	 * @param resources $db
	 */
	public function first($tablename, $where = '',  $order = '', $db = false){
	
		$dbhandle = !$db ? $this->db : $db;
	
		if( !empty($order) ){
			$dbhandle->order_by($order);
		}
	
		if( ! empty($where) ){
			if(is_array($where)){
				$dbhandle->where($where);
			}else{
				$dbhandle->where($where, NULL, false);
			}
		}
		 
		return $dbhandle->from($tablename)->get()->row_array();
	}
	
	public function query($sql, $db = false){
		$db = !$db ? $this->db : $db;
		return $db->query($sql);
	}
	
	/**
	 * 获取分页查询数据
	 * @param string $tablename
	 * @param string $field
	 * @param string $where
	 * @param string $limit
	 * @param string $order
	 * @param string $group
	 * @param resources $db
	 * @return array
	 */
	public function fetch($tablename, $field = '*', $where = '', $limit = '', $order = '', $group = '', $db = false){
	
		if(empty($tablename)){ return FALSE; }
	
		if(empty($field)){ $field = '*'; }
	
		$dbhandle = !$db ? $this->db : $db;
		 
		if( ! empty($where) ){
			if(is_array($where)){
				$dbhandle->where($where);
			}else{
				$dbhandle->where($where, NULL, false);
			}
		}
	
		if( ! empty($limit) ){
			if(strpos($limit, ',')){
				$limitarr = explode(',', $limit);
				$dbhandle->limit($limitarr[1], $limitarr[0]);
			}else{
				$dbhandle->limit($limit);
			}
		}
	
		if($order){
			$dbhandle->order_by($order);
		}
	
		if($group){
			$dbhandle->group_by($group);
		}
	
		$data = $dbhandle->select($field)->get($tablename)->result_array();
		return $data;
	}
	
	/**
	 * 获取查询数据
	 * @param string $tablename
	 * @param string $field
	 * @param string $where
	 * @param string $order
	 * @param resources $db
	 * @return array
	 */
	public function fetchAll($tablename, $field = '*', $where = '', $order = '', $db = false){
	
		if(empty($tablename)){ return FALSE; }
	
		$dbhandle = !$db ? $this->db : $db;
		 
		if( ! empty($where) ){
			if(is_array($where)){
				$dbhandle->where($where);
			}else{
				$dbhandle->where($where, NULL, false);
			}
		}
	
		if($order){
			$dbhandle->order_by($order);
		}
	
		$data = $dbhandle->select($field)->get($tablename)->result_array();
		return $data;
	}
	
	/**
	 * 获取总数
	 * @param string $tablename
	 * @param string|array $where
	 * @param resources $db
	 * @return boolean
	 */
	public function total($tablename, $where = '', $db = false){
	
		if(empty($tablename)){ return false; }
	
		$dbhandle = !$db ? $this->db : $db;
	
		if( ! empty($where) ){
			if(is_array($where)){
				$dbhandle->where($where);
			}else{
				$dbhandle->where($where, NULL, false);
			}
		}
	
		$count = $dbhandle->count_all_results($tablename);
		return $count;
	}
	
	public function insert_id($db = false){
		if(!$db) $db = $this->db;
		 
		return $db->insert_id();
	}
	
	/**
	 * 添加记录
	 * @param String $tablename
	 * @param Array	$data
	 * @return boolean
	 */
	public function insert($tablename, $data, $db = false){
	
		if(!$db) $db = $this->db;
	
		return $db->insert($tablename, $data);
	}
	
	/**
	 * 更新记录
	 * @param String $tablename
	 * @param Array $data
	 * @param Array $where
	 * @return boolean
	 */
	public function update($tablename, $data, $where, $db = false){
		if(!$db) $db = $this->db;
	
		return $db->update($tablename, $data, $where);
	}
	
	/**
	 * 删除记录
	 * @param String $tablename
	 * @param Array $where
	 * @return boolean
	 */
	public function delete($tablename, $where, $db = false){
	
		if(!$db) $db = $this->db;
	
		return $db->delete($tablename, $where);
	}
	
	/**
	 * 主要组装用于对SQL中的IN查询进行SQL查询语句
	 * @param string|array $data
	 * @param array $param 相关组织参数
	 * bool num 参数内所有必须为数字，不是数组不进入IN。默认为true
	 */
	function SQLIN($data, $param = array()){
		$param = array_merge(array('num'=>true), $param);
		if(is_array($data)){
			foreach($data as $k => $v){
				if($param['num'] && !is_numeric($v)){
					unset($data[$k]);
					continue;
				}
				$data[$k] = '\''. $this->escape($v). '\'';
			}
			return implode(',', $data);
		}elseif($param['num'] && !is_numeric($data)){
			return '';
		}else{
			return '\''. $this->escape($data). '\'';
		}
	}
	
	/**
	 * 过滤字串
	 * @param $str string 要过滤的字串
	 * @return string
	 */
	function escape($str) {
		return mysql_escape_string($str);
	}
}