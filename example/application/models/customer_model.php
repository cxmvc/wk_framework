<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 客服系统模型
 * @copyright(c) 2013-2014 www.10000.com
 * @version Id:  company_model.php
 * @author: jyong
 */

class Customer_Model extends db_model{
	public function __construct(){
		parent::__construct();
	}
	
	/**
	 * 获取访客的身份标识
	 * @param int $site_id
	 * @param int $uid
	 * @return string
	 */
	public function test(){
		return $this->fetch('msgs', '*', null, 10);
	}
}