公司项目中WEB项目几乎都是使用codeigniter框架，为了降低PHP的单次执行请求时间、减低服务器处理响应时间，<br>
同时提高每分钟应答的总数，开发这个扩展的目的是将Router、Template、Config、Controller等框架提高的基础通用功能由底层实现，<br>
PHP脚本仅处理业务逻辑，发挥各自的优势。<br>

考虑到项目迁移的成本，所以此扩展的MVC也是基于CI原型来设计的，同时也去除了很多不常用的功能。

支持版本：PHP5.3+
支持Cli模式：

例如：
GET请求http://localhost/api/user?uid=25085, 在Cli下则为$: php ./index.php "c=api&m=user&d=&uid=25085" 

POST请求则为$: php ./index.php "c=api&m=user&d=&uid=25085" -d "post1=1&post2=2"

下面提供了两张截图，1分钟内针对相同网址并发数从10至100的请求测试结果。
<pre>
数据对比：(测试工具curl loader)
类型      1分钟总请求数     成功次数        失败次数    平均响应时间   平均每秒请求数
原CI框架    17706           17706           0           137ms           267次
扩展        57599           57599           0           6ms             866次
</pre>
<br>原PHP Ci框架：
<img src="http://git.oschina.net/365690485/wk_framework/raw/master/%E5%8E%9FCI.jpg"/>

扩展MVC：
<img src="http://git.oschina.net/365690485/wk_framework/raw/master/扩展.jpg"/>

扩展实现的框架，响应速度提高了约10倍。(图片上产生的5XX错误原因是开启PHP5.5自带的opcache在并发测试时出现的，目前还不知道原因，
更换到eAccelerator则没有500)

使用案例：
Nginx.conf配置单入口(与Ci一样单入口一样，没有变动),例如：
<pre>
    server {
        listen 80;
        server_name test.cn;
        index index.php;
        root /usr/local/wwwroot/test/public;

          location / {
                rewrite ^/$ /index.php last;
                #一下是防止某些文件夹被直接访问
                rewrite ^/(?!index\.php|robots\.txt|images|js|css|styles|static)(.*)$ /index.php/$1 last;
          }

          location ~ \.php {
                fastcgi_pass   127.0.0.1:9000;
                fastcgi_index  index.php;
                fastcgi_param  SCRIPT_FILENAME  $document_root$fastcgi_script_name;
                include        fastcgi_params;
          }

    }
</pre>
入口文件index.php:
<pre>
$application_folder = 'application';
$dir_path = './';
if (strpos(__FILE__, '\\') !== false) {
	$dir_path =  substr(__FILE__, 0, strrpos(__FILE__, '\\')) . '/';
} elseif (strpos(__FILE__, '/') !== false) {
	$dir_path =  substr(__FILE__, 0, strrpos(__FILE__, '/')) . '/';
}
define('BASEPATH', str_replace("\\", "/", realpath($dir_path.'../').'/'));
define('APPPATH', realpath(BASEPATH.$application_folder).'/');

$framework = new wk_framework();
$framework->loadView();
$framework->setTemplateDir(APPPATH.'../public/template/');
$framework->setCompileDir(APPPATH.'../public/data/complie/');
$framework->setApplicationPath(APPPATH);

//此处release、test、development，会根据条件自动切换配置文件路径
//优先寻找/application/config/ENVIRONMENT/config，没有在寻找/application/config/config
$framework->setEnvironment('release');
require_once(APPPATH.'core/WK_Controller.php');
$framework->initialize();
$framework->captureRouter();
</pre>